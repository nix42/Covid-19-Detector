# Covid-19-Detector

Detecting covid-19 diagnosis via convolutional neural networks and x-ray images

Completed as part of my final year dissertation titled "COVID-19 diagnosis via convolutional neural network and medical imaging"

## Features

- GUI built using Tkinter
- Convolutional neural network to train the model
- Saved model for fast classification

## Future Features

- Ability to train models on your own system
- Insert multiple x-ray images to train in a queue

## How to run

Clone the repository and run the main.py file to start the UI

## Notes

The dataset folder has only 5 images for each classification, codeberg would not allow me to upload any more than that, below is the dataset used for this project:

- https://www.kaggle.com/nabeelsajid917/covid-19-x-ray-10000-images

