# MODULES REQUIRED FOR MACHINE LEARNING FUNCTIONS
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf 
import cv2
import os

from tensorflow import keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.preprocessing import LabelBinarizer
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from imutils import paths

# MODULES REQUIRED FOR UI FUNCTIONS
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter.simpledialog import askstring

chosenImage = ""
chosenModel = ""

def model_train():
    dataset = []
    labels = []
    imagePaths = list(paths.list_images("dataset"))

    # Resizing and reshaping all images in the dataset
    for imagePath in imagePaths:
        # Get relevent labels based on files folder
        label = imagePath.split(os.path.sep)[-2]

        # Open and load image and resize to 128, 128 width and height
        image = cv2.imread(imagePath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (128, 128))

        # Append images and labels into seperate arrays
        dataset.append(image)
        modelLabels.append(label)

    # Convert to normal arrays
    dataset = np.array(dataset) / 255.0
    modelLabels = np.array(modelLabels)

    # Performs binary operation on label dataset ready for input
    labelBin = LabelBinarizer()
    modelLabels = labelBin.fit_transform(modelLabels)
    modelLabels = to_categorical(modelLabels)

    (train_images, test_images, train_labels, test_labels) = train_test_split(dataset, modelLabels,
        test_size=0.30, stratify=labels, random_state=42)

    # Train neural network CNN
    # Average pooling reduces amount of pixels to scan,
    # in this case 6 by 3 will reduce to 100

    # Maxpool moves the 2 by 2 grid by 2 instead of 1

    # Dropout will drop 50% of connections 
    # to make it seem that we have more training examples

    model = keras.Sequential([ keras.layers.AveragePooling2D(4, 4, input_shape=(128,128, 3)),
        keras.layers.Conv2D(64, 3, activation="relu"),
        keras.layers.Conv2D(32, 3, activation="relu"),
        keras.layers.MaxPool2D(2,2),
        keras.layers.Dropout(0.5),
        keras.layers.Flatten(),
        keras.layers.Dense(128, activation='relu'),
        keras.layers.Dense(2, activation="softmax")
    ])
    # Compiles the ML model using the adam operator and binary cross entropy 
    # as we have 2 outputs (normal or covid)
    model.compile(
        optimizer="adam",
        loss=keras.losses.BinaryCrossentropy(),
        metrics=["accuracy"]
    )
    # Fit the model to the training data to completely iterate over 100 times (epochs)
    model.fit(train_images, train_labels, epochs=100, batch_size=32)
    # Test against the test data and get the level of accuracy
    model.evaluate(test_images, test_labels)

    # Classify an imals with no relevent label 
    predictions = model.predict(test_images, batch_size=32)
    # Converts output to either 1 or 0
    predictions = np.argmax(predictions, axis=1)

    # Output report on accuracy, fscore and prediction
    print(classification_report(test_labels.argmax(axis=1), predictions))

    fileNum = 0
    # Count how many models have been created
    for file in os.listdir("models"):
        fileNum = fileNum + 1

    fileNum += 1
    # Save model based on after previous model number
    model.save("models\model_{}".format(fileNum))

def run_saved_model():
    try:
        saved_model = keras.models.load_model("models\{}".format(chosenModel))

        # Resizing and reshaping the image 
        img = []
        tempImg = cv2.imread(chosenImage)
        tempImg = cv2.cvtColor(tempImg, cv2.COLOR_BGR2RGB)
        tempImg = cv2.resize(tempImg, (128, 128))
        img.append(tempImg)

        # Convert to a numpy array for input
        img = np.array(img) / 255.0
        img = np.reshape(img,[1,128,128,3])
        # run test image on saved model
        test_predict = saved_model.predict(img, batch_size=32)

        test_predict = np.argmax(test_predict, axis=1)

        result = test_predict
        # Call function to display results
        results_screen(result)
    except:
        messagebox.showerror("Model Error", "There is an error when running the model please try again")
        
def choose_image():
    filename = filedialog.askopenfilename(
        title='Open the image for classification',
    )
    global chosenImage
    chosenImage = filename

def choose_model():
    modelDialog = askstring("Model selection", "enter the name of the model")
    global chosenModel
    chosenModel = modelDialog

def run_model():
    print(chosenImage)
    print(chosenModel)
    # Checks whether correct data has been inputted and runs the model
    if chosenImage == "" or chosenModel == "":
        messagebox.showerror("Error with input", "Incorrect or no input has been chosen, please try again")
        return
    else:
        if chosenImage != "" and chosenModel != "":
            run_saved_model()

def help_screen():
    # Basic information on how to use the software
    message = ("The software is simple and easy to use here are intrusctions on how to run the model:\n\n\n" +
                        "1. Run the main.py file\n2. Input the image you want to classify\n3. Select the model you want to use\n" +
                        "4. Click the classify image button at the bottom right of the screen. ")

    messagebox.showinfo("Help Section", message)

def main_screen():
    main = tk.Tk()
    main.title("Covid-19 detector")
    main.geometry("800x600")
    main.resizable(0, 0)
    # UI elements for left side of the interface
    input_image = tk.Frame(master=main, height=350, width=400, bg="grey")
    input_image.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)
    
    inputButton = tk.Button(input_image, text="Select Image", command=choose_image)
    inputButton.pack(side=tk.TOP, pady=100)
    # UI elements for right side of the interface
    select_model = tk.Frame(master=main, height=350, width=400, bg="light grey")
    select_model.pack(expand=True, fill=tk.BOTH, side=tk.RIGHT)

    modelButton = tk.Button(select_model, text="Select model", command=choose_model)
    modelButton.pack(side=tk.TOP, pady=100)

    models = []
    for folder in os.listdir("models"):
        models.append(folder)
    # List of models available to use
    listBox = tk.Listbox(select_model)
    listBox.pack()
    listBox.insert(0, models[0])

    nextButton = tk.Button(select_model, text="Classify Image", command=run_model)
    nextButton.pack(side=tk.BOTTOM, pady=20)

    helpButton = tk.Button(select_model, text="Help", command=help_screen)
    helpButton.pack(side=tk.BOTTOM, pady=20)

    main.mainloop()

def results_screen(result):
    # Displays results for a given input image
    resultStr = ""
    if result == 1:
        resultStr = "Normal (negative)"
    elif result == 0:
        resultStr = "Covid (positive)"

    messagebox.showinfo("result", "The image is: {}\n\nAccuracy: {}\n\nLoss: {}\n\n{}".format(resultStr, "100%", 0.0016,  "based on the classification report from the trained model"))

main_screen()

